# README #

This README would normally document whatever steps are necessary to get your application up and running.

### About App ###

* Canvas Gallery with tags
* Front-end app build by html5 canvas, CSS and Javascript

### Features of the App ###

* Allow users to upload images to a canvas area.
* Add tags by inputting text with modal box.
* Within the canvas element, the user will be able to drag and drop the tags and images.
* Able to upload multiple images. 
* Able to delete tags/images and navigation by clicking next and back.
* Delete All tags by clicking clear all tags
* Mini gallery view that shows thumbnails of all uploaded images. 
* Clicking on the thumbnail will switch to the image.


### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact