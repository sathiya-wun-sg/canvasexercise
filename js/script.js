
var canvas1 = document.getElementById("cvs1");
var contexts = canvas1.getContext('2d');
var itemsIndex = 0;
var selectedIndex = 1;
var stepNumber = 0;
var states = [];
var pageArray = [];
var checkLabelAdded = 0;
var startX = (canvas1.width/3);
var startY = 100;
var clicking = false;

function clearAll() {
    canvas1.width = canvas1.width
}
function handleClick(e, contextIndex) {
    e.stopPropagation();
    var mouseX = parseInt(e.clientX - e.target.offsetLeft);
    var mouseY = parseInt(e.clientY - e.target.offsetTop);
    clearAll();
    for (var i = 0; i < states.length; i++) {
        var state = states[i];
        if (state.dragging) {
            state.dragging = false;
            state.draw();
            continue;
        }
        if (state.contextIndex == contextIndex && mouseX > state.x && mouseX < state.x + state.width && mouseY > state.y && mouseY < state.y + state.height) {
            state.dragging = true;
            state.offsetX = mouseX - state.x;
            state.offsetY = mouseY - state.y;
            state.contextIndex = contextIndex;
        }
        state.draw();
    }
}
function handleMousemove(e, contextIndex) {
    e.stopPropagation();
    var mouseX = parseInt(e.clientX - e.target.offsetLeft);
    var mouseY = parseInt(e.clientY - e.target.offsetTop);
    clearAll();
    for (var i = 0; i < states.length; i++) {
        var state = states[i];
        if (state.dragging) {
            state.x = mouseX - state.offsetX;
            state.y = mouseY - state.offsetY;
            state.contextIndex = contextIndex;
            selectedIndex = state.itemIndex;
        }
        state.draw();
    }
}
canvas1.onmousemove = function (e) {
    if(clicking){ handleMousemove(e, 1); }
}
function addStateimage(x, y, image, index) {
    state = {}
    state.dragging = false;
    state.selected = false;
    state.type = "image";
    state.contextIndex = 1;
    state.itemIndex = index;
    state.image = image;
    state.x = x;
    state.y = y;
    state.width = image.width;
    state.height = image.height;
    state.offsetX = 0;
    state.offsetY = 0;
    state.draw = function () {
        var context = contexts;
        if (this.dragging) {
            if (this.selected) {
                context.strokeStyle = 'red';
            }else{
                context.strokeStyle = 'black';
            }
            context.strokeRect(this.x, this.y, this.width + 5, this.height + 5);
        }
        context.drawImage(this.image, this.x, this.y);                        
    }
    state.draw();
    return (state);
}
function addStatelabel(x, y, text, index) {
    state = {}
    state.dragging = false;
    state.type = "label";
    state.contextIndex = 1;
    state.itemIndex = index;
    state.image = text;
    state.x = x;
    state.y = y;
    state.width = text.width;
    state.height = text.height;
    state.offsetX = 0;
    state.offsetY = 0;
    state.draw = function () {
        var context = contexts;
        if (this.dragging) {
            context.strokeStyle = 'red';
            context.strokeRect(this.x, this.y, this.width + 6, this.height + 6);
        }
        context.font = "20pt Calibri";
        context.fillStyle = 'blue';
        context.strokeStyle = 'black';
        context.fillText(text.text, this.x+3, this.y+30);
    }
    state.draw();
    return (state);
}
function updateStepNumber(pVar){
    (pVar=="push") ? stepNumber = pageArray.length : stepNumber -= 1;
    var stepHtml = stepNumber+' of '+pageArray.length;
    $('#stepNumber').html(stepHtml);
}

function loadNextImage(){   
    pageArray.push(states);
    updateStepNumber("push");
}
function handleImage(e){
  e.preventDefault();  
  let file = [...e.target.files] || [];
  file.forEach((item, index) => {
    let reader = new FileReader();
    reader.onloadend = (event) => {
      var img = new Image();
      img.onload = function(){
        checkLabelAdded = 1;
        if(index == 0){ 
            clearAll(); 
            states = []; 
            clearTags();
        }
        itemsIndex += 1;
        states.push(addStateimage((index*100+startX), startY, img, itemsIndex));
        if(index == (file.length-1)){ loadNextImage(); clearTags(); showAddLabel(); addThumbImage(); }
      }
      img.src = event.target.result;
    };
    
    reader.readAsDataURL(file[index]);
  });
}
function clearTags(){
    $(".add-tags").empty();
}
function removeThumbImage(pCurr){
    $('.thumb-image>img').eq(pCurr).remove();
}
function addThumbImage(){
    state = states[0];
    var thumbImg;
    for (var i = 0; i < states.length; i++) {
        state = states[i];        
        thumbImg = state.image;
    }
    $('.thumb-image').append(thumbImg);
}
function AddTagsToBottom(text, currIndex) {
    $('.add-tags').append('<h2>' + text + ' <button data-curr-index="' + currIndex + '">' + 'Delete</button>' + '</h2>');
}
function deleteCanvasElementLabelAll(){
    for (var i = 0; i < states.length; i++) {
        var state = states[i];
        if (state.type == 'label') {
            states.splice(i, 1);
        }
    }
    reDrawCanvas();
}
function reDrawCanvas(){
    var mouseX = 0;
    var mouseY = 0;
    clearAll();
    for (var i = 0; i < states.length; i++) {
        var state = states[i];
        if (state.dragging) {
            state.x = mouseX - state.offsetX;
            state.y = mouseY - state.offsetY;
        }
        state.draw();
    }
}
function reloadPrevStep(){
    if(stepNumber==1 && pageArray.length>1){
        stepNumber = 1;
        pageArray.splice(stepNumber-1, 1);  
        removeThumbImage(stepNumber-1);
    }else{
        stepNumber--;
        pageArray.splice(stepNumber, 1);
        removeThumbImage(stepNumber);
    }
    if(pageArray[stepNumber-1] !== undefined){
        states = pageArray[stepNumber-1];
    }else{
        states = [];
    }
    
    clearTags();
    updateImageName();    
    var stepHtml = stepNumber+' of '+pageArray.length;
    $('#stepNumber').html(stepHtml);
    reDrawCanvas();   
}

function deleteCanvasElementLabel(pLabelId){
    var findIndex = states.findIndex(x => x.itemIndex === pLabelId);
    states.splice(findIndex, 1);
    reDrawCanvas();
}
function deleteCanvasElementImage(){
    var findIndex = states.findIndex(x => x.type === 'image');
    var statesLength = 0;
    for (var i = 0; i < states.length; i++) {
        var state = states[i];
        if(state.type === 'image'){
            statesLength += 1;
        }
    }        
    if(findIndex>=0){        
        if(statesLength == 1){
            reloadPrevStep();
        }else{
            states.splice(findIndex, 1); 
            reDrawCanvas();
        }
    }
}
function updateImageName(){
    var imageName = ' Image '+ stepNumber;
    $( "#image_name" ).html(imageName);    
}
function showAddLabel(){
    $( "#add_label" ).show();
    updateImageName();
}
$(document).ready(function() {
    var imageLoader = document.getElementById('image_uploads');
    imageLoader.addEventListener('change', handleImage, false);

    $( "#cvs1" ).mousedown(function(e, contextIndex) {
        clicking = true;
        handleClick(e, 1);
    });
    $( "#cvs1" ).mouseup(function(e, contextIndex) {
        clicking = false;
        handleClick(e, 1);
    });

    $( "#add_label" ).hide();
    $(".add-tags").on("click", "button", function(event){
        $(this).parent().remove();
        currId  = Number($(this).attr("data-curr-index"));  
        deleteCanvasElementLabel(currId);
    });
    $(".thumb-image").on("click", "img", function(event){
        var curIndex = Number($(this).index());
        stepNumber = curIndex + 1;
        states = pageArray[curIndex];
        clearTags();
        updateImageName();
        reDrawCanvas();
        var stepHtml = stepNumber+' of '+pageArray.length;
        $('#stepNumber').html(stepHtml);

    });
    $( "#add_label" ).click(function() {
        if(checkLabelAdded == 0){alert("Please upload image!");}
        else{ $('.modal-box').show(); }
        
    });
    $( "#delete_image" ).click(function() {
        if(stepNumber == undefined) return;
        deleteCanvasElementImage();
    });
    $( "#clear_all_tags" ).click(function() {    
        $(".add-tags button").each(function( index ) {
            $(this).parent().remove();
            currId  = Number($(this).attr("data-curr-index"));
        }); 
        deleteCanvasElementLabelAll();
    });
    $( "#add_label_btn" ).click(function() {
        if($('#theText').val()){
            itemsIndex += 1;
            $('.modal-box').hide();
            var text = { text: $("#theText").val(), x: startX, y: 0 };
            contexts.font = "20pt Calibri";
            contexts.fillStyle = 'blue';
            contexts.strokeStyle = 'black';
            text.width = contexts.measureText(text.text).width;
            text.height = 50;
            states.push(addStatelabel(startX, 50, text, itemsIndex));            
            AddTagsToBottom(text.text, itemsIndex);
            $("#theText").val("");
        }
    });

    $( "#next_button").click(function() {
        if(stepNumber<pageArray.length){
            stepNumber++; 
            states = pageArray[stepNumber-1];
            clearTags();
            updateImageName();
        }
        reDrawCanvas();
        var stepHtml = stepNumber+' of '+pageArray.length;
        $('#stepNumber').html(stepHtml);
    });
    $( "#previous_button").click(function() {
        if(stepNumber>1 && stepNumber<=pageArray.length){
            stepNumber--; 
            states = pageArray[stepNumber-1];
            clearTags();
            updateImageName();
        }
        reDrawCanvas();
        var stepHtml = stepNumber+' of '+pageArray.length;
        $('#stepNumber').html(stepHtml);
    });

    $( "#openModal .close" ).click(function() {
        $('.modal-box').hide();
    });
});
